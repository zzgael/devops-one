<?php namespace App\Tests\Repository;

use App\Entity\Click;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;

class ClickRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testRepositoryAddClick()
    {
        $fakeRequest = Request::create('/click', 'POST');
        $fakeRequest->setSession(new Session(new MockArraySessionStorage()));

        $addedClick = $this->entityManager
            ->getRepository(Click::class)
            ->addClick($fakeRequest)
        ;

        $this->assertEquals("127.0.0.1", $addedClick->getIp() );
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }
}